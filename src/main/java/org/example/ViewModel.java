package org.example;

public class ViewModel {
    InputScanner inputScanner = new InputScanner();
    MethodForUser methodForUser = new MethodForUser();

    public int getAnswerForTheSmallestNumberToRandom() {
        int smallestNumberToRandom = 0;
        return smallestNumberToRandom = methodForUser.getFirstNumber(inputScanner.getScanner());
    }

    public int getAnswerForTheLargestNumberToRandom() {
        int largestNumberToRandom = 0;
        return largestNumberToRandom = methodForUser.getSecondNumber(inputScanner.getScanner());
    }

    public void closeApp() {
        inputScanner.closeScanner();
    }
}
