package org.example;

import java.util.Scanner;

public final class InputScanner {

    Scanner scanner = new Scanner(System.in);

    public Scanner getScanner() {
        return scanner;
    }

    public void closeScanner(){
        scanner.close();
    }
}
