package org.example;
import java.util.InputMismatchException;
import java.util.Scanner;

public class MethodForUser {
    int firstNumber = 0;
    int secondNumber = 0;

    public int getFirstNumber(Scanner scanner) {
        boolean inputIntAccept = false;

        while (!inputIntAccept) {
            try{
                System.out.println("Wybierz najmniejszą możliwą liczbe do wylosowania");
                firstNumber = Integer.parseInt(scanner.nextLine());
                inputIntAccept = true;
            } catch (NumberFormatException e){
                System.out.println("To nie jest liczba, spróbuj jeszcze raz");
            }
        }
        return firstNumber;
    }

    public int getSecondNumber(Scanner scanner) {
        boolean inputIntAccept = false;

        while (!inputIntAccept) {
            try{
                System.out.println("Wybierz największą możliwą liczbe do wylosowania");
                secondNumber = Integer.parseInt(scanner.nextLine());

                if(NumberCorrectValidator.isHigherNumberThanFirst(firstNumber, secondNumber)) {
                    inputIntAccept = true;
                } else {
                    System.out.println("Podana liczba jest za niska, spróbuj ponownie" +
                            "\nMinimalna liczba do uruchomienia programu to: " + (firstNumber+1));
                }

            } catch (NumberFormatException e){
                System.out.println("To nie jest liczba, spróbuj jeszcze raz");
            }
        }
        return secondNumber;
    }
}
