package org.example;

public class Main {
    public static void main(String[] args) {
        ViewModel viewModel = new ViewModel();


        int firstNumberToRandom = viewModel.getAnswerForTheSmallestNumberToRandom();
        int secondNumberToRandom = viewModel.getAnswerForTheLargestNumberToRandom();

        System.out.println(firstNumberToRandom + " i " + secondNumberToRandom);
        viewModel.closeApp();

    }
}